library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity SHIFT_REG is
    port (
        INPUT : in unsigned( DATA_WIDTH - 1 downto 0);
        LDA, RESET, CLK, LATCH : in std_logic;
        MODE : in TYPE_MODE;
        
        OUTPUT : out unsigned( DATA_WIDTH - 1 downto 0)
        );
end entity SHIFT_REG;

architecture SHIFT_REG_BODY of SHIFT_REG is
    signal VALUE : unsigned( DATA_WIDTH - 1 downto 0);
begin
    
    SHIFT : process( CLK )
    begin
        if rising_edge(CLK) then
            if RESET = '1' then
                VALUE <= (others => '0');
            elsif LDA = '1' then
                VALUE <= INPUT;
            else 
                case MODE is
                    when NONE =>
                        VALUE <= VALUE;
                    when CYCL =>
                        VALUE (7) <= VALUE(0);
                        VALUE ( 6 downto 0 ) <= VALUE ( 7 downto 1 );
                    when ARIT =>
                        VALUE <= VALUE (7) & VALUE (7) & VALUE (6 downto 1);
                    when LOG =>
                        VALUE <= '0' & VALUE(7 downto 1);
                end case;   
            end if;
        end if;
    end process SHIFT;
    
    STOP : process (LATCH)
    begin
        if LATCH = '1' then
            OUTPUT <= VALUE;
        end if;
    end process STOP;
            

end SHIFT_REG_BODY;
