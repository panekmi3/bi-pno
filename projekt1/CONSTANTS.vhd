package CONSTANTS is
    
    constant DATA_WIDTH : integer := 8;

    type TYPE_MODE is ( NONE, CYCL, ARIT, LOG );
    type TYPE_STATE is ( WAIT_A, LOAD_A, WAIT_L, LOAD_L_L, LOAD_L_C, LOAD_L_A, DELAY_A, DELAY_C, DELAY_L, SHIFT_L, SHIFT_C, SHIFT_A, STOP );
end package CONSTANTS;