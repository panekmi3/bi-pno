// TEST_GEN.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <bitset>
#include <numeric>
#include <bit>
#include <cstdint>

using namespace std;

bitset<8> RSA(int number, int l)
{
	return bitset<8>(number >> l);
}

bitset<8> RSC( int number, int l)
{
	bitset<8> bits(number);
	for (size_t i = 0; i < l; i++)
	{
		int underflow = bits[0];
	
		bits[0] = bits[1];
		bits[1] = bits[2];
		bits[2] = bits[3];
		bits[3] = bits[4];
		bits[4] = bits[5];
		bits[5] = bits[6];
		bits[6] = bits[7];
		bits[7] = underflow;
	}

	return bits;
}

bitset<8> RSL(int number, int l)
{
	bitset<8> bits(number);
	for (size_t i = 0; i < l; i++)
	{

		bits[0] = bits[1];
		bits[1] = bits[2];
		bits[2] = bits[3];
		bits[3] = bits[4];
		bits[4] = bits[5];
		bits[5] = bits[6];
		bits[6] = bits[7];
		bits[7] = 0;
	}

	return bits;
}

int main()
{
    ofstream vstupy, vystupy;
    vstupy.open("vstupy.vec");
	vystupy.open("vystupy.vec");

	for (int mode = 1; mode <= 3; mode++)
	{
		for( int i = -128; i <= 127; i+=20)
			{
				for (int l = 0; l <= 7; l++)
				{
					bitset<8> number(i);
					bitset<8> shift(l);
					bitset<8> shiftResult;

					if (mode == 1) shiftResult = RSA(i, l);
					if (mode == 2) shiftResult = RSL(i, l);
					if (mode == 3) shiftResult = RSC(i, l);

					vstupy << number << " " << shift << " " << mode << endl;
					vystupy << shiftResult << endl;
				}	
			}
	}
	

	return 0;
}