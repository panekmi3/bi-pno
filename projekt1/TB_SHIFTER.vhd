library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;
use STD.TEXTIO.all;
use work.ALL;

entity TB_SHIFTER is
end TB_SHIFTER;

architecture TB_SHIFTER_BODY of TB_SHIFTER is

component SHIFTER is
    port ( 
        INPUT : in unsigned( DATA_WIDTH - 1 downto 0);
        OUTPUT : out unsigned( DATA_WIDTH - 1 downto 0);
        
        BTN0, BTN1, BTN2, BTN3, CLK, RESET_IN : in std_logic
    );
end component SHIFTER;

signal BTN0_TB, BTN1_TB, BTN2_TB, BTN3_TB, CLK_TB, RESET_IN_TB : std_logic;
signal INPUT_TB : unsigned( DATA_WIDTH - 1 downto 0);
signal OUTPUT_TB : unsigned( DATA_WIDTH - 1 downto 0);

constant CLK_PERIOD : time := 10 ns;

begin

    DUT : SHIFTER port map(
        INPUT_TB, OUTPUT_TB, BTN0_TB, BTN1_TB, BTN2_TB, BTN3_TB, CLK_TB, RESET_IN_TB
    );

    CLK_GEN : process
    begin
        CLK_TB <= '0';
        wait for CLK_PERIOD/2;
        CLK_TB <= '1';
        wait for CLK_PERIOD/2;
    end process CLK_GEN;
    
    RESET_GEN : process
    begin
        RESET_IN_TB <= '0';
        wait for 20 ns;
        RESET_IN_TB <= '1';
        wait for 20 ns;
        RESET_IN_TB <= '0';
        wait;
    end process RESET_GEN;
        
        
    STIMULI_GEN : process
          file     VSTUPY       : TEXT is in "c:/vstupy.vec";
          file     VYSTUPY      : TEXT is in "c:/vystupy.vec";
          variable LINE_VSTUPY, LINE_VYSTUPY    : LINE;
          variable A_BV         : BIT_VECTOR ( DATA_WIDTH - 1 downto 0 );
          variable L_BV         : BIT_VECTOR ( 7 downto 0 );
          variable RES_BV       : BIT_VECTOR ( DATA_WIDTH - 1 downto 0 );
          variable A            : STD_LOGIC_VECTOR ( DATA_WIDTH - 1 downto 0 );
          variable L            : STD_LOGIC_VECTOR ( 7 downto 0 );
          variable MODE         : natural;
          variable RES          : STD_LOGIC_VECTOR ( DATA_WIDTH - 1 downto 0 );
       begin
          wait until RESET_IN_TB = '1';
          BTN0_TB <= '0';
          BTN1_TB <= '0';
          BTN2_TB <= '0';
          BTN3_TB <= '0';
          wait until RESET_IN_TB = '0';
          wait for 33 ns;
    
    
          -- testujeme vsechny vstupni hodnoty ze souboru
          while not ENDFILE(VSTUPY) loop
          
             wait until CLK_TB = '1';
             readline(VSTUPY, LINE_VSTUPY);               -- nacteme vstupy ze souboru
             readline(VYSTUPY, LINE_VYSTUPY);
             read(LINE_VSTUPY, A_BV);
             read(LINE_VSTUPY, L_BV);
             read(LINE_VSTUPY, MODE);
             read(LINE_VYSTUPY, RES_BV);
             A := To_StdLogicVector(A_BV);
             L := To_StdLogicVector(L_BV);
             RES := To_StdLogicVector(RES_BV);
    
             INPUT_TB <= unsigned(A);                                   -- zadame operand A
             wait for 23 ns;
             BTN0_TB <= '1';
             wait for 33 ns;
             BTN0_TB <= '0';
    
             wait for 55 ns;
    
             INPUT_TB <= unsigned(L);                                   -- zadame operand L
             wait for 23 ns;
             case MODE is
                when 1 =>
                    BTN1_TB <= '1';
                when 2 =>
                    BTN2_TB <= '1';
                when others =>
                    BTN3_TB <= '1';
             end case;
             wait for 33 ns;
             BTN1_TB <= '0';
             BTN2_TB <= '0';
             BTN3_TB <= '0';
    
             wait for 20*CLK_PERIOD;                          -- pockame na vysledek
    
    
             ---------------------------------------------------
             -- spravne vysledky jsou v externim souboru: 
             -- TB_OUTPUT = ... C
             ---------------------------------------------------
             assert OUTPUT_TB = unsigned( RES )
                report "CHYBA: Vstupy: " & integer'image(TO_INTEGER(SIGNED(A))) & " "&integer'image(TO_INTEGER(SIGNED(L))) & ", Vystup: " & integer'image(TO_INTEGER(SIGNED(OUTPUT_TB))) & "; Ocekavam: " & integer'image(TO_INTEGER(SIGNED(RES))) 
                severity error;
             ---------------------------------------------------
                
          end loop; 
          
          assert FALSE report "KONEC SIMULACE" severity failure;
          -- staci pouze: 
          -- report "KONEC SIMULACE" severity failure;
          
       end process;


end TB_SHIFTER_BODY;
