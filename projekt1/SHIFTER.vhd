library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.CONSTANTS.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SHIFTER is
    port ( 
        INPUT : in unsigned( DATA_WIDTH - 1 downto 0);
        OUTPUT : out unsigned( DATA_WIDTH - 1 downto 0);
        
        COPY_OF_INPUTS : out unsigned( ( DATA_WIDTH * 2 ) - 1 downto 0 );
        
        BTN0, BTN1, BTN2, BTN3, CLK, RESET_IN : in std_logic
    );
end entity SHIFTER;

architecture SHIFTER_BODY of SHIFTER is

component SHIFT_REG is
    port (
        INPUT : in unsigned( DATA_WIDTH - 1 downto 0);
        LDA, RESET, CLK, LATCH: in std_logic;
        MODE : in TYPE_MODE;
        
        OUTPUT : out unsigned( DATA_WIDTH - 1 downto 0)
        );
end component SHIFT_REG;

component L_REG is
    port (
        LDL, RESET, CLK : in std_logic;
        DEC : in std_logic;
        INPUT : in unsigned( 2 downto 0);
        ZERO : out std_logic
    );
end component L_REG;

component CONTROLLER is
    port (
        LDA, LDL, RESET, LATCH, DEC : out std_logic;
        MODE : out TYPE_MODE;
        ZERO, BTN0, BTN1, BTN2, BTN3, CLK, RESET_CON : in std_logic
    );
end component CONTROLLER;

signal LDA, LDL, RESET, ZERO, LATCH, DEC: std_logic;
signal MODE : TYPE_MODE;

begin

    SHIFT_REG_INST : SHIFT_REG port map(
        INPUT => INPUT,
        LDA => LDA,
        RESET => RESET,
        CLK => CLK,
        LATCH => LATCH,
        MODE => MODE,
        OUTPUT => OUTPUT
    );
    
    L_REG_INST : L_REG port map(
        LDL => LDL,
        RESET => RESET,
        CLK => CLK,
        DEC => DEC,
        INPUT => INPUT(2 downto 0),
        ZERO => ZERO
    );
    
    CONTROLLER_INST : CONTROLLER port map(
        LDA => LDA,
        LDL => LDL,
        RESET => RESET,
        LATCH => LATCH,
        DEC => DEC,
        MODE => MODE,
        ZERO => ZERO,
        BTN0 => BTN0,
        BTN1 => BTN1,
        BTN2 => BTN2,
        BTN3 => BTN3,
        CLK => CLK,
        RESET_CON => RESET_IN
    );
    
    COPY_INPUTS : process( CLK )
    begin
    
        if rising_edge(CLK) then
            if LDA = '1' then
                COPY_OF_INPUTS( 7 downto 0 ) <= INPUT;
            elsif LDL = '1' then
                COPY_OF_INPUTS( 15 downto 8 ) <= "00000" & INPUT( 2 downto 0);
            end if;
            
        end if;
    
    end process COPY_INPUTS;
    
end SHIFTER_BODY;
