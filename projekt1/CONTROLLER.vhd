library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity CONTROLLER is
    port (
        LDA, LDL, RESET, LATCH, DEC : out std_logic;
        MODE : out TYPE_MODE;
        ZERO, BTN0, BTN1, BTN2, BTN3, CLK, RESET_CON : in std_logic
    );
end entity CONTROLLER;

architecture CONTROLLER_BODY of CONTROLLER is
    signal STATE, NEXT_STATE : TYPE_STATE;
    --signal INTERNAL_MODE : TYPE_MODE;
begin

    NEXT_STATE_LOGIC : process ( STATE, ZERO, BTN0, BTN1, BTN2, BTN3 )
    begin
        NEXT_STATE <= STATE;
        case STATE is
            when WAIT_A =>
                if BTN0 = '1' then
                    NEXT_STATE <= LOAD_A;
                else
                    NEXT_STATE <= WAIT_A;
                end if;
            when LOAD_A =>
                NEXT_STATE <= WAIT_L;
            when WAIT_L =>
                if BTN1 = '1' then
                    NEXT_STATE <= LOAD_L_A;
                elsif BTN2 = '1' then
                    NEXT_STATE <= LOAD_L_L;
                elsif BTN3 = '1' then
                    NEXT_STATE <= LOAD_L_C;
                else
                    NEXT_STATE <= WAIT_L;
                end if;
            when LOAD_L_L =>
                NEXT_STATE <= DELAY_L;
            when LOAD_L_A =>
                NEXT_STATE <= DELAY_A;
            when LOAD_L_C =>
                NEXT_STATE <= DELAY_C;
            when DELAY_L =>
                NEXT_STATE <= SHIFT_L;
                if ZERO = '1' then
                    NEXT_STATE <= STOP;
                end if;
            when DELAY_A =>
                NEXT_STATE <= SHIFT_A;
                if ZERO = '1' then
                    NEXT_STATE <= STOP;
                end if;
            when DELAY_C =>
                NEXT_STATE <= SHIFT_C;
                if ZERO = '1' then
                    NEXT_STATE <= STOP;
                end if;
            when SHIFT_L =>
                if ZERO = '1' then
                    NEXT_STATE <= STOP;
                end if;
            when SHIFT_A =>
                if ZERO = '1' then
                    NEXT_STATE <= STOP;
                end if;
            when SHIFT_C =>
                if ZERO = '1' then
                    NEXT_STATE <= STOP;
                end if;
            when STOP =>
                NEXT_STATE <= WAIT_A;
        end case;
        
    end process NEXT_STATE_LOGIC;
    
    OUTPUT_LOGIC : process ( STATE )
    begin
        
        LDA <= '0';
        LDL <= '0';
        RESET <= '0';
        MODE <= NONE;
        LATCH <= '0';
        DEC <= '0';
        
        case STATE is
            when WAIT_A =>
                RESET <= '1';
            when LOAD_A =>
                LDA <= '1';
            when WAIT_L =>
                NULL;
            when DELAY_L =>
                DEC <= '1';
            when DELAY_A =>
                DEC <= '1';
            when DELAY_C =>
                DEC <= '1';
            when LOAD_L_L =>
                LDL <= '1';
            when LOAD_L_A =>
                LDL <= '1';
            when LOAD_L_C =>
                LDL <= '1';
            when SHIFT_L =>
                MODE <= LOG;
                DEC <= '1';
            when SHIFT_A =>
                MODE <= ARIT;
                DEC <= '1';
            when SHIFT_C =>
                MODE <= CYCL;
                DEC <= '1';
            when STOP=>
                LATCH <= '1';
        end case;
    end process OUTPUT_LOGIC;
    
    STATE_REG : process(CLK)
    begin
        if rising_edge(CLK) then
            if RESET_CON = '1' then
                STATE <= WAIT_A;
            else
                STATE <= NEXT_STATE;
            end if;
         end if;
    end process STATE_REG;
    
end CONTROLLER_BODY;
