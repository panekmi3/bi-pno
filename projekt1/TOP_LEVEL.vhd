library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity TOP_LEVEL is
    port (
        INPUT : in unsigned( DATA_WIDTH - 1 downto 0);
        BTN0, BTN1, BTN2, BTN3, CLK, RESET : in std_logic;
        SEGMENTS : out std_logic_vector( 6 downto 0);
        DP  : out std_logic;
        DIGIT : out std_logic_vector( 3 downto 0)
    );
end TOP_LEVEL;

architecture TOP_LEVEL_BODY of TOP_LEVEL is

component SHIFTER is
    port ( 
        INPUT : in unsigned( DATA_WIDTH - 1 downto 0);
        OUTPUT : out unsigned( DATA_WIDTH - 1 downto 0);
        
        COPY_OF_INPUTS : out unsigned( ( DATA_WIDTH * 2 ) - 1 downto 0 );
        
        BTN0, BTN1, BTN2, BTN3, CLK, RESET_IN : in std_logic
    );
end component SHIFTER;

component HEX2SEG is
   port (
      DATA     : in  STD_LOGIC_VECTOR (15 downto 0);   -- vstupni data k zobrazeni (4 sestnactkove cislice)
      CLK      : in  STD_LOGIC;
      SEGMENT  : out STD_LOGIC_VECTOR (6 downto 0);    -- 7 segmentu displeje
      DP       : out STD_LOGIC;                        -- desetinna tecka
      DIGIT    : out STD_LOGIC_VECTOR (3 downto 0)     -- 4 cifry displeje
   );
end component HEX2SEG;

signal OUTPUTS : unsigned ( DATA_WIDTH - 1 downto 0 );
signal COPY_OF_INPUTS : unsigned( 15 downto 0);
signal TO_HEX : std_logic_vector ( 15 downto 0);

begin

    MULTIPLEX : process ( OUTPUTS, COPY_OF_INPUTS, BTN3 )
    begin
        if BTN3 = '0' then
            TO_HEX <=  "00000000"  & std_logic_vector(OUTPUTS);
        else
            TO_HEX <= std_logic_vector(COPY_OF_INPUTS);
        end if;
    end process MULTIPLEX;
    
    SHIFTER_ISNT : SHIFTER port map(
        INPUT => INPUT,
        OUTPUT => OUTPUTS,
        COPY_OF_INPUTS => COPY_OF_INPUTS,
        BTN0 => BTN0,
        BTN1 => BTN1,
        BTN2 => BTN2,
        BTN3 => BTN3,
        CLK => CLK,
        RESET_IN => RESET
    );
    
    HEX2SEG_ISNT : HEX2SEG port map(
        DATA => TO_HEX,
        CLK => CLK,
        SEGMENT => SEGMENTS,
        DP => DP,
        DIGIT => DIGIT
    );
   
end TOP_LEVEL_BODY;
