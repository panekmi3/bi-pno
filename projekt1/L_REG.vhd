library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity L_REG is
    port (
        LDL, RESET, CLK : in std_logic;
        DEC : in std_logic;
        INPUT : in unsigned( 2 downto 0);
        ZERO : out std_logic
    );
end entity L_REG;

architecture L_REG_BODY of L_REG is
    signal VALUE : unsigned(2 downto 0);
begin
    
    LOAD : process(CLK)
    begin
        if rising_edge(CLK)then
            if RESET = '1' then
                VALUE <= to_unsigned(7, 3);
            elsif LDL = '1' then
                VALUE <= INPUT;
            elsif  DEC = '1' then
                VALUE <= VALUE - 1;
            end if;
        end if;
    end process LOAD;

    CHECK_ZERO : process (VALUE)
    begin
        if VALUE = 0 then
            ZERO <= '1';
        else
            ZERO <= '0';
        end if;
    end process CHECK_ZERO;
end L_REG_BODY;
