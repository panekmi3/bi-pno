----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.11.2021 15:24:46
-- Design Name: 
-- Module Name: ROTARY_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.uniform;
use ieee.math_real.floor;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.all;

entity ROTARY_TB is
end ROTARY_TB;

architecture Behavioral of ROTARY_TB is

component ROTARY is
   port (
      ROTARY_IN   : in  std_logic_vector (2 downto 0);   -- rotary button  (left, right, push)
      REGX   : out std_logic_vector (7 downto 0);   -- X
      REGY   : out std_logic_vector (7 downto 0);   -- Y
      CLK    : in  std_logic;                       -- 100 MHz
      RESET  : in  std_logic
   );
end component ROTARY;

signal DATA_TB                          : std_logic_vector(2 downto 0);
signal REGX_TB_DUT, REGY_TB_DUT         : std_logic_vector(7 downto 0); 
signal REGX_TB_GOLDEN, REGY_TB_GOLDEN   : std_logic_vector(7 downto 0); 
signal CLK_TB, RESET_TB                 : std_logic; 

for DUT    : ROTARY use entity work.ROTARY(ROTARY_BODY) port map (
                ROTARY_IN    =>  ROTARY_IN,
                REGX    =>  REGX,
                REGY    =>  REGY,
                CLK     =>  CLK,
                RESET   =>  RESET
                );

for GOLDEN  : ROTARY use entity work.ROTARY(SoftwareModel) port map (
                ROTARY_IN    =>  ROTARY_IN,
                REGX    =>  REGX,
                REGY    =>  REGY,
                CLK     =>  CLK,
                RESET   =>  RESET
                );


signal A                    : unsigned(22 downto 0) := "00111111111000000000000";
signal B                    : unsigned(22 downto 0) := "00000001111111110000000";
signal A_bounced            : unsigned(22 downto 0);
signal B_bounced            : unsigned(22 downto 0);
signal Button               : std_logic;
type DIRECTION is (LEFT, RIGHT, NONE);
signal dir                  : DIRECTION             := RIGHT;
signal RANDBIT_A1           : unsigned(0 downto 0);
signal RANDBIT_A2           : unsigned(0 downto 0);
signal RANDBIT_B1           : unsigned(0 downto 0);
signal RANDBIT_B2           : unsigned(0 downto 0);

signal STATE_OUT_TB           : std_logic_vector(4 downto 0);
signal ROTARY_DEBOUNCED_OUT_TB           : std_logic_vector(2 downto 0);
signal SEL_OUT_TB           : std_logic;

begin

    GOLDEN  : ROTARY port map(
                ROTARY_IN    =>  DATA_TB, 
                REGX    =>  REGX_TB_GOLDEN,  
                REGY    =>  REGY_TB_GOLDEN, 
                CLK     =>  CLK_TB, 
                RESET   =>  RESET_TB
             );
             
    DUT  : ROTARY port map(
                ROTARY_IN    =>  DATA_TB, 
                REGX    =>  REGX_TB_DUT,  
                REGY    =>  REGY_TB_DUT, 
                CLK     =>  CLK_TB, 
                RESET   =>  RESET_TB
             );
   
   
    
    DATA_TB <= std_logic(A_bounced(22)) & std_logic(B_bounced(22)) & Button;
    

    BOUNCEGEN1 : process
        variable seed1  : positive := 1;
        variable seed2  : positive := 1;
        variable x      : real;
        variable y      : integer;
    begin
        uniform(seed1, seed2, x);
        y := integer(floor(x * 2.0));
        RANDBIT_A1 <= to_unsigned(y, 1);
        uniform(seed1, seed2, x);
        y := integer(floor(x * 2.0));
        RANDBIT_A2 <= to_unsigned(y, 1);
        uniform(seed1, seed2, x);
        y := integer(floor(x * 2.0));
        RANDBIT_B1 <= to_unsigned(y, 1);
        uniform(seed1, seed2, x);
        y := integer(floor(x * 2.0));
        RANDBIT_B2 <= to_unsigned(y, 1);
        wait for 100 ns;
    end process BOUNCEGEN1;
    
    CLKGEN : process
    begin
        CLK_TB <= '0';
        wait for 10 ns;
        CLK_TB <= '1';
        wait for 10 ns;
    end process CLKGEN;
    
    ROTATE : process
    begin
        for i in 0 to 100 loop
            if dir = LEFT then
                A_bounced <= (RANDBIT_A1 & (5 downto 0=>'1') & RANDBIT_A2 & (14 downto 0=>'0')) ror (23-i);
                B_bounced <= ((4 downto 0=>'0') & RANDBIT_B1 & (4 downto 0=>'1') & RANDBIT_B2 & (10 downto 0=>'0')) ror (23-i);
            elsif dir = RIGHT then
                A_bounced <= (RANDBIT_A1 & (5 downto 0=>'1') & RANDBIT_A2 & (14 downto 0=>'0')) ror i;
                B_bounced <= ((4 downto 0=>'0') & RANDBIT_B1 & (4 downto 0=>'1') & RANDBIT_B2 & (10 downto 0=>'0')) ror i;
            else
                A <= A;
                B <= B;
            end if;

            
            wait for 10 ns;
        
            assert REGX_TB_DUT = REGX_TB_GOLDEN
            report "Error: X coords are not equal!"
            severity error;
            
            assert REGY_TB_DUT = REGY_TB_GOLDEN 
            report "Error: Y coords are not equal!"
            severity error;
            
        
        end loop;
    end process ROTATE;

    STIMULI_GEN : process
    begin
        RESET_TB <= '1';
        wait for 40 ns;
        RESET_TB <= '0';
        Button  <= '0';
        wait for 40 ns;
    
        dir     <= RIGHT;
        wait for 10000 us;
        dir     <= LEFT;
        wait for 5000 us;
        
        Button  <= '1';
        wait for 100 ms;
        Button  <= '0';
        dir     <= RIGHT;
        wait for 10000 us;
        dir     <= LEFT;
        wait for 5000 us;
        
        dir     <= NONE;
        wait for 1000 us;
        
        assert FALSE report "KONEC SIMULACE" severity failure;

    end process STIMULI_GEN;

end Behavioral;
