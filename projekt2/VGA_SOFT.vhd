library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity VGA is
port (
    REGX      : in  std_logic_vector (7 downto 0); -- X
    REGY      : in  std_logic_vector (7 downto 0); -- Y
    VGA_RED   : out std_logic;
    VGA_GREEN : out std_logic;
    VGA_BLUE  : out std_logic;
    VGA_HSYNC : out std_logic;
    VGA_VSYNC : out std_logic;
    CLK       : in  std_logic;                     -- 100 MHz
    RESET     : in  std_logic
);
end VGA;

architecture SOFTWARE_MODEL of VGA is
begin
    
    PIXEL_DATA : process
        --800 x 525
    variable X, Y : integer := 0;
    begin
        
        if RESET = '1' then
            X := 0;
            Y := 0;
        end if;
    
        VGA_GREEN <= '0';
        VGA_RED <= '0';
        VGA_BLUE <= '0';
        VGA_HSYNC <= '1';
        VGA_VSYNC <= '1';
        
        X := X + 1;
        if X = 800 then
            X := 0;
            Y := Y + 1;
            if Y = 525 then
                Y := 0;
            end if;
        end if;
        
        if std_logic_vector( to_unsigned(X, 8)) = REGX or std_logic_vector( to_unsigned(Y, 8)) = REGY then
            VGA_GREEN <= '1';
            VGA_RED <= '1';
            VGA_BLUE <= '1';
        end if;
        
        if X >= VISIBLE_AREA_H then 
            VGA_GREEN <= '0';
            VGA_RED <= '0';
            VGA_BLUE <= '0';
        end if;
        
        if X >= VISIBLE_AREA_H + FRONT_PORCH_H and X <= VISIBLE_AREA_H + FRONT_PORCH_H  + SYNC_PULSE_H then
            VGA_HSYNC <= '0';
        end if;
                
        if Y >= VISIBLE_AREA_V + FRONT_PORCH_V and Y <= VISIBLE_AREA_V + FRONT_PORCH_V  + SYNC_PULSE_V then
            VGA_VSYNC <= '0';
        end if;
        
        wait for PIXEL_TIME;
        
    end process PIXEL_DATA;

end SOFTWARE_MODEL;

architecture VGA_BODY of VGA is
begin

end VGA_BODY;