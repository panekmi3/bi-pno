library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SELECTOR is
    port (
        INPUT : in std_logic;
        OUTPUT : out std_logic
    );
end entity SELECTOR;

architecture Behavioral of SELECTOR is
    signal LATCH : std_logic;
begin

LATCH <= not LATCH when rising_edge(INPUT);

OUTPUT <= LATCH;

end Behavioral;
