library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ENCODER is
   port (
       ROTARY : in  std_logic_vector (1 downto 0);   -- rotary button  (left, right, push)
       REGX   : out std_logic_vector (7 downto 0);   -- X
       REGY   : out std_logic_vector (7 downto 0);   -- Y
       CLK    : in  std_logic;                       -- 100 MHz
       RESET, REG_SEL  : in  std_logic
    );
end entity ENCODER;

architecture ENCODER_BODY of ENCODER is

type stateType is (START, WAITING, RIGHT1, RIGHT2, RIGHT3, PLUS, LEFT1, LEFT2, LEFT3, MINUS);

signal STATE, NEXTSTATE : stateType;
signal X, Y : unsigned(7 downto 0);
signal SELECTED : std_logic; -- '0' -> X, '1' -> Y
signal INCREMENT, DECREMENT, RESET_COORD : std_logic;
begin

NEXTSTATELOGIC : process(STATE, ROTARY)
begin
    case STATE is
        when WAITING =>
            if ROTARY(1) = '0' then
                NEXTSTATE <= RIGHT1;
            elsif ROTARY(0) = '0' then
                NEXTSTATE <= LEFT1;
            else 
                NEXTSTATE <= WAITING;
             end if;
        when RIGHT1 =>
            if ROTARY(1) = '1' then
                NEXTSTATE <= WAITING;
            elsif ROTARY(0) = '0' then
                NEXTSTATE <= RIGHT2;
            else 
                NEXTSTATE <= RIGHT1;
            end if;
        when RIGHT2 =>
            if ROTARY(0) = '1' then
                NEXTSTATE <= RIGHT1;
            elsif ROTARY(1) = '1' then
                NEXTSTATE <= RIGHT3;
            else 
                NEXTSTATE <= RIGHT2;
            end if;
        when RIGHT3 =>
            if ROTARY(1) = '0' then
                NEXTSTATE <= RIGHT2;
            elsif ROTARY(0) = '1' then
                NEXTSTATE <= PLUS;
            else 
                NEXTSTATE <= RIGHT3;
            end if;
        when LEFT1 =>
            if ROTARY(0) = '1' then
                NEXTSTATE <= WAITING;
            elsif ROTARY(1) = '0' then
                NEXTSTATE <= LEFT2;
            else
                NEXTSTATE <= LEFT1;
            end if;
        when LEFT2 =>
            if ROTARY(1) = '1' then
                NEXTSTATE <= LEFT1;
            elsif ROTARY(0) = '1' then
                NEXTSTATE <= LEFT3;
            else
                NEXTSTATE <= LEFT2;
            end if;
        when LEFT3 =>
            if ROTARY(0) = '0' then
                NEXTSTATE <= LEFT2;
            elsif ROTARY(1) = '1' then
                NEXTSTATE <= MINUS;
            else
                NEXTSTATE <= LEFT3;
            end if;
        when others =>
            NEXTSTATE <= WAITING;
        
    end case;
end process;

OUTPUTLOGIC : process( STATE )
begin
    if STATE = START then
        RESET_COORD <= '1';
    elsif STATE = PLUS then
        INCREMENT <= '1';
    elsif STATE = MINUS then
        DECREMENT <= '1';
    else
        RESET_COORD <= '0';
        INCREMENT <= '0';    
        DECREMENT <= '0';
    end if;
 end process;

REG_DRIVE : process( CLK )
begin
    if rising_edge(CLK) then    
        if RESET_COORD = '1' then
            X <= ( others => '0' );
            Y <= ( others => '0' );
        elsif REG_SEL = '0' then
            if INCREMENT = '1' then
                X <= X + 1;
            elsif DECREMENT = '1' then
                X <= X - 1;
            end if;
        else
            if INCREMENT = '1' then
                Y <= Y + 1;
            elsif DECREMENT = '1' then
                Y <= Y - 1;
            end if;
        end if;
    end if;
end process REG_DRIVE;

STATEREG : process( CLK )
begin
    if rising_edge(CLK) then
        if RESET = '1' then
            STATE <= START;
        else 
            STATE <= NEXTSTATE;
        end if;
    end if;
end process;

REGX <= std_logic_vector(X);
REGY <= std_logic_vector(Y);

end ENCODER_BODY;
