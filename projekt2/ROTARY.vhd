library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ROTARY is
   port (
       ROTARY_IN : in  std_logic_vector (2 downto 0);   -- rotary button  (left, right, push)
       REGX   : out std_logic_vector (7 downto 0);   -- X
       REGY   : out std_logic_vector (7 downto 0);   -- Y
       CLK    : in  std_logic;                       -- 100 MHz
       RESET  : in  std_logic
    );
end entity ROTARY;

architecture ROTARY_BODY of ROTARY is

   component ENCODER is
   port (
       ROTARY : in  std_logic_vector (1 downto 0);   -- rotary button  (left, right, push)
       REGX   : out std_logic_vector (7 downto 0);   -- X
       REGY   : out std_logic_vector (7 downto 0);   -- Y
       CLK    : in  std_logic;                       -- 100 MHz
       RESET, REG_SEL  : in  std_logic
    );
    end component ENCODER;
    
    component DEBOUNCE is
    generic( MODULO : integer ); 
        port ( 
            CLK, RESET : in std_logic;
            INPUT : in std_logic;
            OUTPUT : out std_logic
        );
    end component DEBOUNCE;

    component SELECTOR is
        port (
            INPUT : in std_logic;
            OUTPUT : out std_logic
        );
    end component SELECTOR;

    signal ROTARY_DEBOUNCED : std_logic_vector( 2 downto 0 );
    signal REG_SEL : std_logic;

begin

    ENCODER_INST : ENCODER port map( ROTARY_DEBOUNCED( 1 downto 0), REGX, REGY, CLK, RESET, REG_SEL);
    SELECTOR_INST : SELECTOR port map( ROTARY_DEBOUNCED(2), REG_SEL );
    DEBOUNCE_A : DEBOUNCE generic map ( MODULO => 100) port map( CLK, RESET, ROTARY_IN(0), ROTARY_DEBOUNCED(0) ); --1 us
    DEBOUNCE_B : DEBOUNCE generic map ( MODULO => 100) port map( CLK, RESET, ROTARY_IN(1), ROTARY_DEBOUNCED(1) ); --1 us
    DEBOUNCE_BTN : DEBOUNCE generic map ( MODULO => 1000000) port map( CLK, RESET, ROTARY_IN(2), ROTARY_DEBOUNCED(2) ); --10 ms
    
end ROTARY_BODY;

architecture SoftwareModel of ROTARY is
    signal X, Y                 : UNSIGNED (7 downto 0);
    type DIRECTIONS             is (LEFT, RIGHT, NONE);
    type T_STATE                is (S11, S10, S01, L00, R00, L00_emit, R00_emit);
    signal XDIR                 : DIRECTIONS := NONE;
    signal YDIR                 : DIRECTIONS := NONE; 
    signal TMP                  : DIRECTIONS;
    signal STATE, NEXT_STATE    : T_STATE;
    signal SEL                  : std_logic;
    signal INPUT                : std_logic_vector(1 downto 0);
    signal BUTTON               : std_logic;
    signal BTNPREV              : std_logic;
begin


REGX    <= STD_LOGIC_VECTOR(X);
REGY    <= STD_LOGIC_VECTOR(Y);
INPUT   <= ROTARY_IN(1 downto 0);
BUTTON  <= ROTARY_IN(2);

XREG : process(CLK)
begin   
    if CLK = '1' and CLK'event then
        if RESET = '1' then
            X <= (7 downto 0=>'0');
        else
            case XDIR is
                when LEFT   =>  if X > 0 then
                                    X <= X - 1; 
                                end if;
                when RIGHT  =>  if X < 254 then
                                    X <= X + 1;
                                end if;         
                when others =>  X <= X;
            end case;
        end if;
    end if;
end process XREG;

YREG : process(CLK)
begin   
    if rising_edge(CLK) then
        if RESET = '1' then
            Y <= (7 downto 0=>'0');
        else
            case YDIR is
                when LEFT   =>  if Y > 0 then
                                    Y <= Y - 1; 
                                end if;
                when RIGHT  =>  if Y < 254 then
                                    Y <= Y + 1;
                                end if;         
                when others =>  Y <= Y;
            end case;
        end if;
    end if;
end process YREG;

DETECT : process(STATE, INPUT)
begin
    case STATE is
            when S11        =>  if INPUT = "10" then
                                    NEXT_STATE <= S10;
                                elsif INPUT = "01" then
                                    NEXT_STATE <= S01;
                                end if;
            when S10        =>  if INPUT = "11" then
                                    NEXT_STATE <= S11;
                                elsif INPUT = "00" then
                                    NEXT_STATE <= R00;
                                end if;
            when S01        =>  if INPUT = "00" then
                                    NEXT_STATE <= L00;
                                elsif INPUT = "11" then
                                    NEXT_STATE <= S11;
                                end if;
            when L00        =>  if INPUT = "10" then
                                    NEXT_STATE <= L00_emit;
                                elsif INPUT = "11" then
                                    NEXT_STATE <= S01;
                                end if;
            when R00        =>  if INPUT = "01" then
                                    NEXT_STATE <= R00_emit;
                                elsif INPUT = "10" then
                                    NEXT_STATE <= S10;
                                end if;
            when L00_emit   =>  NEXT_STATE <= S10;
            when R00_emit   =>  NEXT_STATE <= S01;
        end case;
end process DETECT;

OUTPUT : process(STATE)
begin
    case STATE is
        when L00_emit   => TMP <= LEFT;
        when R00_emit   => TMP <= RIGHT;
        when others     => TMP <= NONE;
    end case;
end process OUTPUT;

STATEP : process(CLK)
begin
    if CLK = '1' and CLK'event then
        if RESET = '1' then
            case INPUT is
                when "00"   => STATE <= R00;
                when "01"   => STATE <= S10;
                when "10"   => STATE <= S01;
                when "11"   => STATE <= S11;
                when others => STATE <= S11;
            end case;
        else
            STATE <= NEXT_STATE;
        end if;
    end if;
end process STATEP;

SELP : process(SEL, TMP)
begin
    if SEL = '1' then
        YDIR <= TMP;
    else
        XDIR <= TMP;
    end if;
end process SELP;

SELCHANGE : process(CLK)
begin
    if CLK = '1' and CLK'event then
        if RESET = '1' then
            SEL         <= '0';
            BTNPREV     <= '0';
        else
            BTNPREV <= BUTTON;
            if ((BTNPREV = not(BUTTON)) and BUTTON = '1') then
                SEL <= not SEL;
            end if;
        end if;
    end if;
end process SELCHANGE;


end architecture SoftwareModel;