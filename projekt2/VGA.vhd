----------------------------------------------------------------------------------
-- Company: CVUT - FIT
-- Engineer: Mariukha Yaroslav (mariuyar@cvut.cz)
-- 
-- Create Date: 04.12.2021 18:06:48
-- Design Name: VGA & ROTARY
-- Module Name: VGA - Behavioral
-- Project Name: VGA and ROTARY
-- Target Devices: Basys3
-- Tool Versions: 
-- Description: 
-- generates vga signal depending on generic parameters
-- Dependencies: 
-- set right input clock frequency
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 1) Task:
-- https://courses.fit.cvut.cz/BI-PNO/labs/07/project22a.html
-- 2) Video Timings: VGA, SVGA, 720p, 1080p:
-- https://projectf.io/posts/video-timings-vga-720p-1080p/
-- 3) I use this image:
-- https://i.stack.imgur.com/6wpSA.jpg
-- 4) Basys 3� FPGA Board Reference Manual:
-- https://digilent.com/reference/_media/basys3:basys3_rm.pdf architecture 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGA2 is
generic(
    H_Active_Pixels       : UNSIGNED;
    H_Front_Porch         : UNSIGNED;
    H_Sync_Width          : UNSIGNED;
    H_Back_Porch          : UNSIGNED;
    H_Blanking_Total      : UNSIGNED;
    H_Total_Pixels        : UNSIGNED;
    H_Cursor_Width        : UNSIGNED;
    V_Active_Lines        : UNSIGNED;
    V_Front_Porch         : UNSIGNED;
    V_Sync_Width          : UNSIGNED;
    V_Back_Porch          : UNSIGNED;
    V_Blanking_Total      : UNSIGNED;
    V_Total_Lines         : UNSIGNED;
    V_Cursor_Width        : UNSIGNED
);
   port (
      REGX      : in  std_logic_vector (7 downto 0)             := "00000000"; -- X
      REGY      : in  std_logic_vector (7 downto 0)             := "00000000"; -- Y
      VGA_RED   : out std_logic_vector (3 downto 0);
      VGA_GREEN : out std_logic_vector (3 downto 0);
      VGA_BLUE  : out std_logic_vector (3 downto 0);
      VGA_HSYNC : out std_logic;
      VGA_VSYNC : out std_logic;
      CLK       : in  std_logic;
      RESET     : in  std_logic                                 := '0'
   );
end VGA2;

architecture Behavioral of VGA2 is
    signal RESET_SW             : STD_LOGIC                     := '0';
    signal RSTCNT               : STD_LOGIC_VECTOR(5 downto 0);
    signal RST                  : STD_LOGIC;
    signal RESET_STATE          : STD_LOGIC_VECTOR(1 downto 0)  := "00";

    signal Rbit, Gbit, Bbit     : STD_LOGIC;
    signal Vcnt                 : UNSIGNED(15 downto 0)         := (15 downto 0=>'0');
    signal Hcnt                 : UNSIGNED(15 downto 0)         := (15 downto 0=>'0');
begin

    RST         <= RESET_SW or RESET;
    VGA_RED     <= (3 downto 0=>Rbit);
    VGA_GREEN   <= (3 downto 0=>Gbit);
    VGA_BLUE    <= (3 downto 0=>Bbit);
    

    -- GENERATE RESET --
    RESET_GEN : process( CLK )
    begin
        if CLK'event and CLK = '1' then
            case RESET_STATE is
                when "00" =>    if RSTCNT(5) = '1' then
                                    RESET_STATE <= "01";
                                end if;
                                RESET_SW    <= '0';

                when "01" =>    RESET_SW    <= '1';
                                RESET_STATE <= "10";
                
                when "10" =>    RESET_SW    <= '0';
                when others =>  RESET_STATE <= "00";
                                RESET_SW    <= '0';
            end case;
        end if;
    end process RESET_GEN;
    -----

    -- V H counters --
    VHCOUNTERS : process( CLK )
    begin
        if CLK'event and CLK = '1' then
            if RST = '1' then
                Vcnt <= V_Total_Lines;
                Hcnt <= H_Total_Pixels;
            else
                if Hcnt = H_Total_Pixels then
                    Hcnt <= to_unsigned(1, 16);
                    if Vcnt = V_Total_Lines then
                        Vcnt <= to_unsigned(1, 16);
                    else
                        Vcnt <= Vcnt + 1;
                    end if;
                else
                    Hcnt <= Hcnt + 1;
                end if;
            end if;
        end if;
    end process VHCOUNTERS;
    -----

    -- hsync --
    HSYNCH_GEN : process( CLK )
    begin
        if CLK'event and CLK = '1' then
            if RST = '1' then
                VGA_HSYNC <= '1';
            else
            if Hcnt >= (H_Active_Pixels + H_Front_Porch) and Hcnt < (H_Active_Pixels + H_Front_Porch + H_Sync_Width) then
                    VGA_HSYNC <= '0';
                else
                    VGA_HSYNC <= '1';
                end if;
            end if;
        end if;
    end process HSYNCH_GEN;
    -----

    -- vsync --
    VSYNCH_GEN : process( CLK )
    begin
        if CLK'event and CLK = '1' then
            if RST = '1' then
                VGA_VSYNC <= '1';
            else
            if (Vcnt >= (V_Active_Lines + V_Front_Porch)) and (Vcnt < (V_Active_Lines + V_Front_Porch + V_Sync_Width)) then
                    VGA_VSYNC <= '0';
                else
                    VGA_VSYNC <= '1';
                end if;
            end if;
        end if;
    end process VSYNCH_GEN;
    -----

    -- video data --
    VIDEO_MUX : process( REGX, REGY, Hcnt, Vcnt )
    begin
        if Hcnt = unsigned(REGX) or Vcnt = unsigned(REGY) or Hcnt = to_unsigned(1, 16) or Hcnt = H_Active_Pixels or Vcnt = to_unsigned(1, 16) or Vcnt = V_Active_Lines then
            Rbit <= '1';
            Gbit <= '1';
            Bbit <= '1';
        else
            Rbit <= '0';
            Gbit <= '0';
            Bbit <= '0';
        end if;
    end process VIDEO_MUX;
    -----


end Behavioral;
