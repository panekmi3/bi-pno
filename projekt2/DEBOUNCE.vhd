library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DEBOUNCE is
generic( MODULO : integer );
    port ( 
        CLK, RESET : in std_logic;
        INPUT : in std_logic;
        OUTPUT : out std_logic
    );
end entity DEBOUNCE;

architecture DEBOUNCE_BODY of DEBOUNCE is

    signal COUNTER : unsigned( 31 downto 0 );
    signal SAMPLE, RESET_COUNT, DEBOUNCED : std_logic;

begin

    COUNT : process(CLK)
    begin
        if rising_edge(CLK) then
            if RESET_COUNT = '1' or RESET = '1' then
                COUNTER <= (others => '0');
            else
                COUNTER <= COUNTER + 1;
            end if;
        end if;
    end process COUNT;
    
    DEBOUNCE : process(COUNTER)
    begin
        if COUNTER = to_unsigned( MODULO, 32) then --11110100001001000000 - 10 ms, 11000011010100000 - 1 ms , 10011100010000 - 0.1 ms
            
            if SAMPLE = INPUT then
                OUTPUT <= INPUT;
            end if;
            
            SAMPLE <= INPUT;
            
            RESET_COUNT <= '1';
        else
            RESET_COUNT <= '0';
        end if;
    end process DEBOUNCE;
    
end DEBOUNCE_BODY;