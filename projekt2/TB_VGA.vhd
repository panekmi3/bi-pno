library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity TB_VGA is
end entity TB_VGA;

architecture TB_VGA_BODY of TB_VGA is
    
    constant CLK_PERIOD : time := 10 ns;
    
    component VGA is
           port (
           REGX      : in  std_logic_vector (7 downto 0); -- X
           REGY      : in  std_logic_vector (7 downto 0); -- Y
           VGA_RED   : out std_logic;
           VGA_GREEN : out std_logic;
           VGA_BLUE  : out std_logic;
           VGA_HSYNC : out std_logic;
           VGA_VSYNC : out std_logic;
           CLK       : in  std_logic;                     -- 100 MHz
           RESET     : in  std_logic
        );
     end component VGA ;
    
    signal REGX_TB, REGY_TB : std_logic_vector (7 downto 0);
    signal VGA_RED_TB, VGA_GREEN_TB, VGA_BLUE_TB, VGA_HSYNC_TB, VGA_VSYNC_TB, CLK_TB, RESET_TB : std_logic;
    signal VGA_RED_TB_GOLDEN, VGA_GREEN_TB_GOLDEN, VGA_BLUE_TB_GOLDEN, VGA_HSYNC_TB_GOLDEN, VGA_VSYNC_TB_GOLDEN: std_logic;
    
    signal VSYNC_HAPENED : std_logic := '0';

    --                                    SOFTWARE_MODEL vym�nit za opravdovou implementaci!!
     for DUT    : VGA use entity work.VGA(SOFTWARE_MODEL) port map (
             REGX => REGX_TB,
             REGY => REGY_TB,
             VGA_RED => VGA_RED_TB,
             VGA_GREEN => VGA_GREEN_TB,
             VGA_BLUE  => VGA_BLUE_TB,
             VGA_HSYNC => VGA_HSYNC_TB,
             VGA_VSYNC => VGA_VSYNC_TB,
             CLK => CLK_TB,                  -- 100 MHz
             RESET => RESET_TB
     );
     
     -- architektura SOFTWARE_MODEL obsahuje zlaty standard - generator spravnych vysledku                             
     for GOLDEN    : VGA use entity work.VGA(SOFTWARE_MODEL) port map (
                REGX => REGX_TB,
                REGY => REGY_TB,
                VGA_RED => VGA_RED_TB_GOLDEN,
                VGA_GREEN => VGA_GREEN_TB_GOLDEN,
                VGA_BLUE  => VGA_BLUE_TB_GOLDEN,
                VGA_HSYNC => VGA_HSYNC_TB_GOLDEN,
                VGA_VSYNC => VGA_VSYNC_TB_GOLDEN,
                CLK => CLK_TB,                  -- 100 MHz
                RESET => RESET_TB
        );
begin

DUT    : VGA port map (
    REGX => REGX_TB,
    REGY => REGY_TB,
    VGA_RED => VGA_RED_TB,
    VGA_GREEN => VGA_GREEN_TB,
    VGA_BLUE  => VGA_BLUE_TB,
    VGA_HSYNC => VGA_HSYNC_TB,
    VGA_VSYNC => VGA_VSYNC_TB,
    CLK => CLK_TB,                  -- 100 MHz
    RESET => RESET_TB
);

GOLDEN    : VGA port map (
    REGX => REGX_TB,
    REGY => REGY_TB,
    VGA_RED => VGA_RED_TB_GOLDEN,
    VGA_GREEN => VGA_GREEN_TB_GOLDEN,
    VGA_BLUE  => VGA_BLUE_TB_GOLDEN,
    VGA_HSYNC => VGA_HSYNC_TB_GOLDEN,
    VGA_VSYNC => VGA_VSYNC_TB_GOLDEN,
    CLK => CLK_TB,                  -- 100 MHz
    RESET => RESET_TB
);

CLK_GEN : process
begin

    CLK_TB <= '0';
    wait for CLK_PERIOD / 2;
    CLK_TB <= '1';
    wait for CLK_PERIOD / 2;
    
end process CLK_GEN;

RESET_GEN : process
begin
    
    RESET_TB <= '0';
    wait for 10 ns;
    RESET_TB <= '1';
    wait for 50 ns;
    RESET_TB <= '0';
    wait;
    
end process RESET_GEN;

VALUES_GEN : process
begin

    wait until RESET_TB = '1';
    REGX_TB <= (others => '0');
    REGY_TB <= (others => '0');
    wait until RESET_TB = '0';
    
    for A in 0 to 255 loop
        for B in 0 to 255 loop
            
            REGX_TB <= std_logic_vector(to_unsigned( A, 8));
            REGY_TB <= std_logic_vector(to_unsigned( B, 8));
            
            wait for PIXEL_TIME * 800 * 525; --wait one frame
            
        end loop;
    end loop;
    
end process VALUES_GEN;

WAIT_FOR_FIRST_SYNC: process
begin
    wait until VGA_VSYNC_TB = '0';
    VSYNC_HAPENED <= '1';
    wait;

end process;

TEST : process
begin
    wait until VSYNC_HAPENED = '1';
    
    while true loop
    
        assert VGA_RED_TB = VGA_RED_TB_GOLDEN and VGA_GREEN_TB = VGA_GREEN_TB_GOLDEN and VGA_BLUE_TB = VGA_BLUE_TB_GOLDEN and VGA_HSYNC_TB = VGA_HSYNC_TB_GOLDEN and VGA_VSYNC_TB = VGA_VSYNC_TB_GOLDEN
        report "CHYBA v �ase: " & time'image(now)
        severity error;
        
        wait for PIXEL_TIME / 2;
    end loop;

    
end process TEST;

end TB_VGA_BODY;
