library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TOP is
    port ( 
        ROTARY_IN : in std_logic_vector(2 downto 0);
        CLK, RESET :  in std_logic;
        
        SEGMENTS : out std_logic_vector(7 downto 0);
        DIGIT : out std_logic_vector(3 downto 0);
        
        VGA_RED         : out std_logic_vector (3 downto 0);
        VGA_GREEN       : out std_logic_vector (3 downto 0);
        VGA_BLUE        : out std_logic_vector (3 downto 0);
        VGA_HSYNC       : out std_logic;
        VGA_VSYNC       : out std_logic
    );
end TOP;

architecture Behavioral of TOP is

    component VGA2 is
        generic(
            H_Active_Pixels       : UNSIGNED :=  to_unsigned(1920, 16);
            H_Front_Porch         : UNSIGNED :=  to_unsigned(  88, 16);
            H_Sync_Width          : UNSIGNED :=  to_unsigned(  44, 16);
            H_Back_Porch          : UNSIGNED :=  to_unsigned( 148, 16);
            H_Blanking_Total      : UNSIGNED :=  to_unsigned( 280, 16);
            H_Total_Pixels        : UNSIGNED :=  to_unsigned(2200, 16);
            H_Cursor_Width        : UNSIGNED :=  to_unsigned(  20, 16);
            V_Active_Lines        : UNSIGNED :=  to_unsigned(1080, 16);
            V_Front_Porch         : UNSIGNED :=  to_unsigned(   4, 16);
            V_Sync_Width          : UNSIGNED :=  to_unsigned(   5, 16);
            V_Back_Porch          : UNSIGNED :=  to_unsigned(  36, 16);
            V_Blanking_Total      : UNSIGNED :=  to_unsigned(  45, 16);
            V_Total_Lines         : UNSIGNED :=  to_unsigned(1125, 16);
            V_Cursor_Width        : UNSIGNED :=  to_unsigned(  20, 16)
        );
        port (
            REGX      : in  std_logic_vector (7 downto 0);
            REGY      : in  std_logic_vector (7 downto 0);
            VGA_RED   : out std_logic_vector (3 downto 0);
            VGA_GREEN : out std_logic_vector (3 downto 0);
            VGA_BLUE  : out std_logic_vector (3 downto 0);
            VGA_HSYNC : out std_logic;
            VGA_VSYNC : out std_logic;
            CLK       : in  std_logic;
            RESET     : in  std_logic
        );
    end component VGA2;

    component clk_wiz_0 is 
    port (
        clk_in1     : in std_logic;
        reset       : in std_logic;
        clk_out1    : out std_logic
    );
    end component clk_wiz_0;





    component ROTARY is
       port (
           ROTARY_IN : in  std_logic_vector (2 downto 0);   -- rotary button  (left, right, push)
           REGX   : out std_logic_vector (7 downto 0);   -- X
           REGY   : out std_logic_vector (7 downto 0);   -- Y
           CLK    : in  std_logic;                       -- 100 MHz
           RESET  : in  std_logic;
           
           STATE_OUT : out std_logic_vector( 4 downto 0 );
           ROTARY_DEBOUNCED_OUT : out std_logic_vector (2 downto 0);
           
           SEL_OUT : out std_logic
        );
    end component ROTARY;
    
    component HEX2SEG is
       port (
          DATA     : in  STD_LOGIC_VECTOR (15 downto 0);   -- vstupni data k zobrazeni (4 sestnactkove cislice)
          CLK      : in  STD_LOGIC;
          SEGMENT  : out STD_LOGIC_VECTOR (6 downto 0);    -- 7 segmentu displeje
          DP       : out STD_LOGIC;                        -- desetinna tecka
          DIGIT    : out STD_LOGIC_VECTOR (3 downto 0)     -- 4 cifry displeje
       );
    end component HEX2SEG;
        
    --signal ROTARY_DEBOUNCED : std_logic_vector(2 downto 0);
    signal DISPLAY_DATA : std_logic_vector(15 downto 0);
    
    for ROTARY_INST : ROTARY use entity work.ROTARY(ROTARY_BODY) port map (
                    ROTARY_IN    =>  ROTARY_IN,
                    REGX    =>  REGX,
                    REGY    =>  REGY,
                    CLK     =>  CLK,
                    RESET   =>  RESET
                    );
    
    
    
    signal CLK_VGA          : std_logic;
    signal VGA_RED_sig      : std_logic_vector (3 downto 0);
    signal VGA_GREEN_sig    : std_logic_vector (3 downto 0);
    signal VGA_BLUE_sig     : std_logic_vector (3 downto 0);
    signal VGA_HSYNC_sig    : std_logic;
    signal VGA_VSYNC_sig    : std_logic;
    signal DATA             : std_logic_vector (7 downto 0);
    
begin
    ROTARY_INST : ROTARY port map( ROTARY_IN, DISPLAY_DATA( 15 downto 8 ), DISPLAY_DATA( 7 downto 0 ), CLK, RESET);
    DISPLAY : HEX2SEG port map( DISPLAY_DATA, CLK, SEGMENTS( 6 downto 0 ), SEGMENTS(7), DIGIT );

    VGA_RED    <= VGA_RED_sig;
    VGA_GREEN  <= VGA_GREEN_sig;
    VGA_BLUE   <= VGA_BLUE_sig;
    VGA_HSYNC  <= VGA_HSYNC_sig;
    VGA_VSYNC  <= VGA_VSYNC_sig;
    
    vga_1920x1080_60Hz : VGA2 
    generic map( 
        H_Active_Pixels       => to_unsigned(1920, 16),
        H_Front_Porch         => to_unsigned(  88, 16),
        H_Sync_Width          => to_unsigned(  44, 16),
        H_Back_Porch          => to_unsigned( 148, 16),
        H_Blanking_Total      => to_unsigned( 280, 16),
        H_Total_Pixels        => to_unsigned(2200, 16),
        H_Cursor_Width        => to_unsigned(  20, 16),
        V_Active_Lines        => to_unsigned(1080, 16),
        V_Front_Porch         => to_unsigned(   4, 16),
        V_Sync_Width          => to_unsigned(   5, 16),
        V_Back_Porch          => to_unsigned(  36, 16),
        V_Blanking_Total      => to_unsigned(  45, 16),
        V_Total_Lines         => to_unsigned(1125, 16),
        V_Cursor_Width        => to_unsigned(  20, 16)
        )
    port map(
        REGX      => DISPLAY_DATA( 15 downto 8 ),          -- in
        REGY      => DISPLAY_DATA( 7 downto 0 ),          -- in
        VGA_RED   => VGA_RED_sig,   -- out
        VGA_GREEN => VGA_GREEN_sig, -- out
        VGA_BLUE  => VGA_BLUE_sig,  -- out
        VGA_HSYNC => VGA_HSYNC_sig, -- out
        VGA_VSYNC => VGA_VSYNC_sig, -- out
        CLK       => CLK_VGA,       -- in
        RESET     => RESET          -- in
        );
    
    clk148_5_GEN : clk_wiz_0 port map(
        clk_in1     => CLK,
        reset       => RESET,
        clk_out1    => CLK_VGA
    );

end Behavioral;
